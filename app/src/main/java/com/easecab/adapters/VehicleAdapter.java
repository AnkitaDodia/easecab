package com.easecab.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.easecab.R;
import com.easecab.activities.MainActivity;
import com.easecab.activities.MyBookingsActivity;
import com.easecab.models.Booking;
import com.easecab.models.Vehicles;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankita on 04-Jul-17.
 */
public class VehicleAdapter extends BaseAdapter
{
    MainActivity mContext;

    Typeface font;

    ArrayList<Vehicles> mList = new ArrayList<>();
    List<Integer> mVehicleSelList = new ArrayList<>();

    private LayoutInflater layoutInflater;

    public VehicleAdapter(MainActivity mContext, ArrayList<Vehicles> list, List<Integer> VehicleSelList) {

        this.mContext = mContext;
        this.mList = list;
        this.mVehicleSelList = VehicleSelList ;


        font = mContext.setLatoRegularFont();


        layoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView;

        rootView = layoutInflater.inflate(R.layout.car_selection_item, null, true);

        Vehicles data = mList.get(position);

        ImageView car_image = (ImageView) rootView.findViewById(R.id.car_image);
        ImageView img_car_sel = (ImageView) rootView.findViewById(R.id.img_car_sel);

        TextView car_name = (TextView) rootView.findViewById(R.id.car_name);
        TextView textView_passenger = (TextView) rootView.findViewById(R.id.textView_passenger);
        TextView textView_bags = (TextView) rootView.findViewById(R.id.textView_bags);

        car_name.setTypeface(mContext.setLatoSemiBoldFont());
        textView_passenger.setTypeface(mContext.setLatoRegularFont());
        textView_bags.setTypeface(mContext.setLatoRegularFont());

        car_name.setText(data.getVehicleType());
        textView_passenger.setText(data.getVehiclePassenger()+" PEOPLE");
        textView_bags.setText(data.getVehicleHandLuggage()+" BAGS");

        if(mVehicleSelList.get(position) == 1){

            img_car_sel.setVisibility(View.VISIBLE);

        }else{

            img_car_sel.setVisibility(View.INVISIBLE);
        }

        Glide.with(mContext).load(data.getImagepath()).placeholder(R.drawable.car).into(car_image);
        return rootView;
    }
}
