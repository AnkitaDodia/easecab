package com.easecab.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easecab.R;
import com.easecab.activities.MyBookingsActivity;
import com.easecab.models.Booking;

import java.util.ArrayList;

/**
 * Created by Ankita on 01-Jul-17.
 */
public class BookingAdapter extends BaseAdapter
{
    MyBookingsActivity mContext;

    Typeface font;

    ArrayList<Booking> mList = new ArrayList<>();

    private LayoutInflater layoutInflater;

    public BookingAdapter(MyBookingsActivity mContext, ArrayList<Booking> list) {

        this.mContext = mContext;
        this.mList = list;
        font = mContext.setLatoRegularFont();
        layoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView;

        rootView = layoutInflater.inflate(R.layout.bookings_list_item, null, true);

        TextView textView_id = (TextView) rootView.findViewById(R.id.textView_id);
        TextView textView_status = (TextView) rootView.findViewById(R.id.textView_status);
        TextView textView_date_time = (TextView) rootView.findViewById(R.id.textView_date_time);
        TextView textView_pickup_booking = (TextView) rootView.findViewById(R.id.textView_pickup_booking);
        TextView textView_drop_off = (TextView) rootView.findViewById(R.id.textView_drop_off);

        LinearLayout main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);

        Booking data = mList.get(position);

        textView_id.setText(data.getBookingId());
        textView_status.setText(data.getBookingStatus());
        textView_date_time.setText(data.getBookingPickUpDateTime());
        textView_pickup_booking.setText(data.getBookingPickUp());
        textView_drop_off.setText(data.getBookingDropOff());

        main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return rootView;
    }
}
