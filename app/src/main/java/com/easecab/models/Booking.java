package com.easecab.models;

/**
 * Created by Ankita on 01-Jul-17.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Booking implements Serializable{
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("booking_returnId")
    @Expose
    private Object bookingReturnId;
    @SerializedName("booking_name")
    @Expose
    private String bookingName;
    @SerializedName("booking_phoneNo")
    @Expose
    private String bookingPhoneNo;
    @SerializedName("booking_email")
    @Expose
    private String bookingEmail;
    @SerializedName("booking_someoneName")
    @Expose
    private String bookingSomeoneName;
    @SerializedName("booking_someonePhoneNo")
    @Expose
    private String bookingSomeonePhoneNo;
    @SerializedName("booking_someoneEmail")
    @Expose
    private String bookingSomeoneEmail;
    @SerializedName("booking_vehicleType")
    @Expose
    private String bookingVehicleType;
    @SerializedName("booking_passengers")
    @Expose
    private String bookingPassengers;
    @SerializedName("booking_checkInLuggage")
    @Expose
    private String bookingCheckInLuggage;
    @SerializedName("booking_handLuggage")
    @Expose
    private String bookingHandLuggage;
    @SerializedName("booking_pickUpDateTime")
    @Expose
    private String bookingPickUpDateTime;
    @SerializedName("booking_pickUp")
    @Expose
    private String bookingPickUp;
    @SerializedName("booking_pickUpPostcode")
    @Expose
    private String bookingPickUpPostcode;
    @SerializedName("booking_dropOff")
    @Expose
    private String bookingDropOff;
    @SerializedName("booking_dropOffPostcode")
    @Expose
    private String bookingDropOffPostcode;
    @SerializedName("booking_driverPickUp")
    @Expose
    private String bookingDriverPickUp;
    @SerializedName("booking_displayName")
    @Expose
    private String bookingDisplayName;
    @SerializedName("booking_flightNo")
    @Expose
    private String bookingFlightNo;
    @SerializedName("booking_flightComingFrom")
    @Expose
    private String bookingFlightComingFrom;
    @SerializedName("booking_extra")
    @Expose
    private String bookingExtra;
    @SerializedName("booking_price")
    @Expose
    private String bookingPrice;
    @SerializedName("booking_paymentType")
    @Expose
    private String bookingPaymentType;
    @SerializedName("booking_paymentStatus")
    @Expose
    private String bookingPaymentStatus;
    @SerializedName("booking_discountCode")
    @Expose
    private String bookingDiscountCode;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("booking_userId")
    @Expose
    private String bookingUserId;
    @SerializedName("booking_createDate")
    @Expose
    private String bookingCreateDate;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Object getBookingReturnId() {
        return bookingReturnId;
    }

    public void setBookingReturnId(Object bookingReturnId) {
        this.bookingReturnId = bookingReturnId;
    }

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public String getBookingPhoneNo() {
        return bookingPhoneNo;
    }

    public void setBookingPhoneNo(String bookingPhoneNo) {
        this.bookingPhoneNo = bookingPhoneNo;
    }

    public String getBookingEmail() {
        return bookingEmail;
    }

    public void setBookingEmail(String bookingEmail) {
        this.bookingEmail = bookingEmail;
    }

    public String getBookingSomeoneName() {
        return bookingSomeoneName;
    }

    public void setBookingSomeoneName(String bookingSomeoneName) {
        this.bookingSomeoneName = bookingSomeoneName;
    }

    public String getBookingSomeonePhoneNo() {
        return bookingSomeonePhoneNo;
    }

    public void setBookingSomeonePhoneNo(String bookingSomeonePhoneNo) {
        this.bookingSomeonePhoneNo = bookingSomeonePhoneNo;
    }

    public String getBookingSomeoneEmail() {
        return bookingSomeoneEmail;
    }

    public void setBookingSomeoneEmail(String bookingSomeoneEmail) {
        this.bookingSomeoneEmail = bookingSomeoneEmail;
    }

    public String getBookingVehicleType() {
        return bookingVehicleType;
    }

    public void setBookingVehicleType(String bookingVehicleType) {
        this.bookingVehicleType = bookingVehicleType;
    }

    public String getBookingPassengers() {
        return bookingPassengers;
    }

    public void setBookingPassengers(String bookingPassengers) {
        this.bookingPassengers = bookingPassengers;
    }

    public String getBookingCheckInLuggage() {
        return bookingCheckInLuggage;
    }

    public void setBookingCheckInLuggage(String bookingCheckInLuggage) {
        this.bookingCheckInLuggage = bookingCheckInLuggage;
    }

    public String getBookingHandLuggage() {
        return bookingHandLuggage;
    }

    public void setBookingHandLuggage(String bookingHandLuggage) {
        this.bookingHandLuggage = bookingHandLuggage;
    }

    public String getBookingPickUpDateTime() {
        return bookingPickUpDateTime;
    }

    public void setBookingPickUpDateTime(String bookingPickUpDateTime) {
        this.bookingPickUpDateTime = bookingPickUpDateTime;
    }

    public String getBookingPickUp() {
        return bookingPickUp;
    }

    public void setBookingPickUp(String bookingPickUp) {
        this.bookingPickUp = bookingPickUp;
    }

    public String getBookingPickUpPostcode() {
        return bookingPickUpPostcode;
    }

    public void setBookingPickUpPostcode(String bookingPickUpPostcode) {
        this.bookingPickUpPostcode = bookingPickUpPostcode;
    }

    public String getBookingDropOff() {
        return bookingDropOff;
    }

    public void setBookingDropOff(String bookingDropOff) {
        this.bookingDropOff = bookingDropOff;
    }

    public String getBookingDropOffPostcode() {
        return bookingDropOffPostcode;
    }

    public void setBookingDropOffPostcode(String bookingDropOffPostcode) {
        this.bookingDropOffPostcode = bookingDropOffPostcode;
    }

    public String getBookingDriverPickUp() {
        return bookingDriverPickUp;
    }

    public void setBookingDriverPickUp(String bookingDriverPickUp) {
        this.bookingDriverPickUp = bookingDriverPickUp;
    }

    public String getBookingDisplayName() {
        return bookingDisplayName;
    }

    public void setBookingDisplayName(String bookingDisplayName) {
        this.bookingDisplayName = bookingDisplayName;
    }

    public String getBookingFlightNo() {
        return bookingFlightNo;
    }

    public void setBookingFlightNo(String bookingFlightNo) {
        this.bookingFlightNo = bookingFlightNo;
    }

    public String getBookingFlightComingFrom() {
        return bookingFlightComingFrom;
    }

    public void setBookingFlightComingFrom(String bookingFlightComingFrom) {
        this.bookingFlightComingFrom = bookingFlightComingFrom;
    }

    public String getBookingExtra() {
        return bookingExtra;
    }

    public void setBookingExtra(String bookingExtra) {
        this.bookingExtra = bookingExtra;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getBookingPaymentType() {
        return bookingPaymentType;
    }

    public void setBookingPaymentType(String bookingPaymentType) {
        this.bookingPaymentType = bookingPaymentType;
    }

    public String getBookingPaymentStatus() {
        return bookingPaymentStatus;
    }

    public void setBookingPaymentStatus(String bookingPaymentStatus) {
        this.bookingPaymentStatus = bookingPaymentStatus;
    }

    public String getBookingDiscountCode() {
        return bookingDiscountCode;
    }

    public void setBookingDiscountCode(String bookingDiscountCode) {
        this.bookingDiscountCode = bookingDiscountCode;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getBookingUserId() {
        return bookingUserId;
    }

    public void setBookingUserId(String bookingUserId) {
        this.bookingUserId = bookingUserId;
    }

    public String getBookingCreateDate() {
        return bookingCreateDate;
    }

    public void setBookingCreateDate(String bookingCreateDate) {
        this.bookingCreateDate = bookingCreateDate;
    }
}
