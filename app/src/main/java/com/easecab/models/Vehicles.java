package com.easecab.models;

/**
 * Created by Ankita on 03-Jul-17.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vehicles {
    @SerializedName("vehicle_id")
    @Expose
    private String vehicleId;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_name")
    @Expose
    private String vehicleName;
    @SerializedName("vehicle_passenger")
    @Expose
    private String vehiclePassenger;
    @SerializedName("vehicle_checkInLuggage")
    @Expose
    private String vehicleCheckInLuggage;
    @SerializedName("vehicle_handLuggage")
    @Expose
    private String vehicleHandLuggage;
    @SerializedName("vehicle_image")
    @Expose
    private String vehicleImage;
    @SerializedName("vehicle_order")
    @Expose
    private String vehicleOrder;
    @SerializedName("vehicle_status")
    @Expose
    private String vehicleStatus;
    @SerializedName("vehicle_createDate")
    @Expose
    private String vehicleCreateDate;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehiclePassenger() {
        return vehiclePassenger;
    }

    public void setVehiclePassenger(String vehiclePassenger) {
        this.vehiclePassenger = vehiclePassenger;
    }

    public String getVehicleCheckInLuggage() {
        return vehicleCheckInLuggage;
    }

    public void setVehicleCheckInLuggage(String vehicleCheckInLuggage) {
        this.vehicleCheckInLuggage = vehicleCheckInLuggage;
    }

    public String getVehicleHandLuggage() {
        return vehicleHandLuggage;
    }

    public void setVehicleHandLuggage(String vehicleHandLuggage) {
        this.vehicleHandLuggage = vehicleHandLuggage;
    }

    public String getVehicleImage() {
        return vehicleImage;
    }

    public void setVehicleImage(String vehicleImage) {
        this.vehicleImage = vehicleImage;
    }

    public String getVehicleOrder() {
        return vehicleOrder;
    }

    public void setVehicleOrder(String vehicleOrder) {
        this.vehicleOrder = vehicleOrder;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getVehicleCreateDate() {
        return vehicleCreateDate;
    }

    public void setVehicleCreateDate(String vehicleCreateDate) {
        this.vehicleCreateDate = vehicleCreateDate;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }
}
