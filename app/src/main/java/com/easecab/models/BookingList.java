package com.easecab.models;

/**
 * Created by Ankita on 01-Jul-17.
 */
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingList {

    @SerializedName("data")
    @Expose
    private ArrayList<Booking> data = null;
    @SerializedName("response")
    @Expose
    private String response;

    public ArrayList<Booking> getData() {
        return data;
    }

    public void setData(ArrayList<Booking> data) {
        this.data = data;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}