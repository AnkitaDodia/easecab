package com.easecab.models;

/**
 * Created by Ankita on 03-Jul-17.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VehicleList {

    @SerializedName("data")
    @Expose
    private ArrayList<Vehicles> data = null;
    @SerializedName("response")
    @Expose
    private String response;

    public ArrayList<Vehicles> getData() {
        return data;
    }

    public void setData(ArrayList<Vehicles> data) {
        this.data = data;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}