package com.easecab.ui;

/**
 * Created by Adite-Ankita on 19-Oct-16.
 */
import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout {

    public TouchableWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
//                MainActivity.mMapIsTouched = false;
                break;

            case MotionEvent.ACTION_UP:
//                MainActivity.mMapIsTouched = true;
                break;

            case MotionEvent.ACTION_MOVE:
//                MainActivity.mMapIsTouched = false;
//                MainActivity.markerClicked = false;
//                MasterActivity.place = null;
                break;
        }
        return super.dispatchTouchEvent(event);
    }
}