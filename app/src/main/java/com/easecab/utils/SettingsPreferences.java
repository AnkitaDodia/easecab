package com.easecab.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.easecab.models.Data;
import com.easecab.models.Login;

import java.io.Serializable;

public class SettingsPreferences implements Serializable {

	private static final String PREFS_NAME = "EaseCabDB";

	private static final String DEFAULT_VAL = null;

	private static final String Key_user_id = "user_id";
	private static final String Key_user_name = "user_fullName";
	private static final String Key_user_email = "user_email";
	private static final String Key_user_mobile = "user_phoneNo";
	private static final String Key_user_address = "user_address";
	private static final String Key_user_postcode = "user_postcode";
	private static final String Key_country = "user_country";

	public static void clearDB(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();
		dbEditor.clear();
		dbEditor.commit();

	}

	public static void storeConsumer(Context context, Login user) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();

		Data data = user.getData();

		dbEditor.putString(Key_user_id, data.getUserId());
		dbEditor.putString(Key_user_name, data.getUserFullName());
		dbEditor.putString(Key_user_email, data.getUserEmail());
		dbEditor.putString(Key_user_mobile, data.getUserPhoneNo());
		dbEditor.putString(Key_user_address, data.getUserAddress());
		dbEditor.putString(Key_user_postcode, data.getUserPostcode());
		dbEditor.putString(Key_country, data.getUserCountry());

		dbEditor.commit();
	}

	public static Login getConsumer(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		Login user = new Login();

		Data data = new Data();

		data.setUserAddress(prefs.getString(Key_user_address, DEFAULT_VAL));
		data.setUserCountry(prefs.getString(Key_country, DEFAULT_VAL));
		data.setUserEmail(prefs.getString(Key_user_email, DEFAULT_VAL));
		data.setUserFullName(prefs.getString(Key_user_name, DEFAULT_VAL));
		data.setUserId(prefs.getString(Key_user_id, DEFAULT_VAL));
		data.setUserPhoneNo(prefs.getString(Key_user_mobile, DEFAULT_VAL));
		data.setUserPostcode(prefs.getString(Key_user_postcode, DEFAULT_VAL));

		user.setData(data);
		return user;
	}
}
