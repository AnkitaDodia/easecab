package com.easecab.common;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.easecab.activities.MainActivity;
import com.easecab.models.Booking;
import com.easecab.models.Vehicles;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by Ankita on 21-Apr-17.
 */
public class MasterActivity extends AppCompatActivity
{
    public static float zoom;

    public static final String TAG = MainActivity.class.getSimpleName();

    public static double latitude,longitude;

    public static String mAddressOutput;

    public static float zoomLevel;

    public static Place place;

    public static LatLng latLng = null;

    ProgressBar pb;

    public static ArrayList<Booking> masterCurrentBookings = new ArrayList<>();
    public static ArrayList<Booking> masterCompleteBookings = new ArrayList<>();
    public static ArrayList<Vehicles> vehicleData = new ArrayList<>();

    public void saveLogin(int loginId)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("LOGIN_ID",loginId);
        spe.commit();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int id = sp.getInt("LOGIN_ID", 0);
        return id;
    }

    public Typeface setLatoRegularFont()
    {
        Typeface tf = Typeface.createFromAsset(getAssets(), "Lato_Regular.ttf");
        return tf;
    }

    public Typeface setLatoBoldFont()
    {
        Typeface tf = Typeface.createFromAsset(getAssets(), "Lato_Bold.ttf");
        return tf;
    }

    public Typeface setLatoSemiBoldFont()
    {
        Typeface tf = Typeface.createFromAsset(getAssets(),"Lato_Semibold.ttf");
        return tf;
    }

    public ProgressBar getPB()
    {
        return pb;
    }

    public void showWaitIndicator(boolean state) {
        if (state)
        {
            ViewGroup layout = (ViewGroup) this.findViewById(
                    android.R.id.content).getRootView();
            pb = new ProgressBar(this, null,
                    android.R.attr.progressBarStyleLarge);

            pb.setVisibility(View.VISIBLE);

            RelativeLayout rl = new RelativeLayout(this);
            rl.setGravity(Gravity.CENTER);
            rl.addView(pb);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);

            layout.addView(rl, params);
        }
        else
        {
            pb.setVisibility(View.GONE);
        }
    }
}
