package com.easecab.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.easecab.R;
import com.easecab.common.MasterActivity;
import com.easecab.models.ForgotPassword;
import com.easecab.models.Login;
import com.easecab.restinterface.RestInterface;
import com.easecab.utils.InternetStatus;
import com.easecab.utils.SettingsPreferences;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Ankita on 21-Apr-17.
 */
public class LoginActivity extends MasterActivity
{
    EditText edit_email,edit_pass;

    Button button_login,button_signUp;

    TextView forgot_password,textView_or,textView_signUp;

    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        isInternet = new InternetStatus().isInternetOn(this);

        initView();
        setClicks();
    }

    private void initView() {
        edit_email = (EditText) findViewById(R.id.edit_email);
        edit_pass = (EditText) findViewById(R.id.edit_pass);

        button_login = (Button) findViewById(R.id.button_login);
        button_signUp = (Button) findViewById(R.id.button_signUp);

        forgot_password = (TextView) findViewById(R.id.forgot_password);
        textView_or = (TextView) findViewById(R.id.textView_or);
        textView_signUp = (TextView) findViewById(R.id.textView_signUp);

        edit_email.setTypeface(setLatoRegularFont());
        edit_pass.setTypeface(setLatoRegularFont());
        button_login.setTypeface(setLatoRegularFont());
        button_signUp.setTypeface(setLatoRegularFont());
        forgot_password.setTypeface(setLatoRegularFont());
        textView_or.setTypeface(setLatoRegularFont());
        textView_signUp.setTypeface(setLatoRegularFont());
    }

    private void setClicks() {
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_email.getText().toString().length() == 0) {
                    Toast.makeText(LoginActivity.this, "Please enter email id", Toast.LENGTH_LONG).show();
                }
                else if(edit_pass.getText().toString().length() == 0)
                {
                    Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_LONG).show();
                }
                else {
                    if (isInternet) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(LoginActivity.this, "No internet connection found", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        button_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(it);
            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(it);
            }
        });
    }

    private void sendLoginRequest()
    {
        showWaitIndicator(true);

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS); // connect timeout

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL)
                .setClient(new OkClient(client)).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.sendLoginRequest(edit_email.getText().toString(), edit_pass.getText().toString(),new Callback<Login>() {
            @Override
            public void success(Login model, Response response) {
                showWaitIndicator(false);

                if (response.getStatus() == 200) {
                    try {
                        Log.e("LOGIN_RESPONSE", "" + new Gson().toJson(model));
                        SettingsPreferences.storeConsumer(LoginActivity.this, model);
                        saveLogin(1);

                        Toast.makeText(LoginActivity.this,"Login successfully",Toast.LENGTH_LONG).show();

                        Intent it = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(it);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());

                if (error.equals(null) || error.getMessage().contains("Unable to resolve host")) {
                    Toast.makeText(LoginActivity.this, "Something went wrong, please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getMessage().contains("failed to connect")) {
                    Toast.makeText(LoginActivity.this, "Sorry can not connect to server please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getResponse().getStatus() == 423) {
                    Toast.makeText(LoginActivity.this, error.getResponse().getReason(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
