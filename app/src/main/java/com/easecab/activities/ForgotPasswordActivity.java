package com.easecab.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.easecab.R;
import com.easecab.common.MasterActivity;
import com.easecab.models.ForgotPassword;
import com.easecab.restinterface.RestInterface;
import com.easecab.utils.InternetStatus;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Ankita on 21-Apr-17.
 */
public class ForgotPasswordActivity extends MasterActivity
{
    ImageButton forgot_back;

    TextView textView_forgot_title,textView_forgot_password_str;

    EditText edit_email_forgot;

    Button button_forgot;

    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        isInternet = new InternetStatus().isInternetOn(this);

        initView();
        setClick();
    }

    private void initView()
    {
        forgot_back = (ImageButton) findViewById(R.id.back);

        textView_forgot_title = (TextView) findViewById(R.id.textView_title);
        textView_forgot_password_str = (TextView) findViewById(R.id.textView_forgot_password_str);

        edit_email_forgot = (EditText) findViewById(R.id.edit_email_forgot);

        button_forgot = (Button) findViewById(R.id.button_forgot);

        textView_forgot_title.setText(R.string.forgot_pass);

        textView_forgot_title.setTypeface(setLatoRegularFont());
        textView_forgot_password_str.setTypeface(setLatoRegularFont());
        edit_email_forgot.setTypeface(setLatoRegularFont());
        button_forgot.setTypeface(setLatoRegularFont());
    }

    private void setClick() {

        button_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_email_forgot.getText().toString().length() == 0) {
                    Toast.makeText(ForgotPasswordActivity.this, "Please enter email id", Toast.LENGTH_LONG).show();
                } else {
                    if (isInternet) {
                        sendForgotPasswordRequest();
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, "No internet connection found", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        forgot_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void sendForgotPasswordRequest()
    {
        showWaitIndicator(true);

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS); // connect timeout

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL)
                .setClient(new OkClient(client)).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.sendForgotPassRequest(edit_email_forgot.getText().toString(),new Callback<ForgotPassword>() {
            @Override
            public void success(ForgotPassword model, Response response) {
                showWaitIndicator(false);

                if (response.getStatus() == 200) {
                    try {
                        Log.e("Forgot_RESPONSE", "" + new Gson().toJson(model));

                        Toast.makeText(ForgotPasswordActivity.this,model.getResponse().toString(),Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());

                if (error.equals(null) || error.getMessage().contains("Unable to resolve host")) {
                    Toast.makeText(ForgotPasswordActivity.this, "Something went wrong, please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getMessage().contains("failed to connect")) {
                    Toast.makeText(ForgotPasswordActivity.this, "Sorry can not connect to server please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getResponse().getStatus() == 423) {
                    Toast.makeText(ForgotPasswordActivity.this, error.getResponse().getReason(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
