package com.easecab.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.easecab.R;
import com.easecab.adapters.VehicleAdapter;
import com.easecab.common.MasterActivity;
import com.easecab.models.VehicleList;
import com.easecab.models.Vehicles;
import com.easecab.restinterface.RestInterface;
import com.easecab.service.FetchAddressIntentService;
import com.easecab.ui.CustomTypefaceSpan;
import com.easecab.ui.DrawerArrowDrawable;
import com.easecab.utils.Constants;
import com.easecab.utils.InternetStatus;
import com.easecab.utils.SettingsPreferences;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends MasterActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener
{

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;

    boolean doubleBackToExitPressedOnce = false;

    private DrawerArrowDrawable drawerArrowDrawable;

    Resources resources;

    private float offset;
    boolean flipped;

    ImageView imageView_fb,imageView_twitter,imageView_gPlus,imageView_linkedIn,imageView_insta;

    //For address
    TextView textView_pickup_title,textView_pickup;
    LinearLayout pickup_layout;
    //For Address

    //For bottom layout
    LinearLayout layout_book_now,layout_book_later;
    //For bottom layout

    // Variables for current location on google map
    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationManager locationManager;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    int w, h;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1,PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP = 2,PLACE_AUTOCOMPLETE_REQUEST_CODE_DROP = 3;


    //book now layout
    LinearLayout book_now_layout_top,layout_select,layout_select_car,layout_return,layout_book_car,layout_return_date_time,
            pickup_layout_book_now,drop_layout_book_now;
    ImageButton back_book_now;
    TextView textView_pickup_title_book_now,textView_pickup_book_now,textView_drop_title_book_now,textView_drop_book_now,
            dateTime_textView_title,select_textView_title,select_car_title,textView_passenger,textView_total_fare_title,
            textView_total_fare,textView_book_car_title,textView_return_back_title,textView_time,textView_date;

    int DatePickerMode;
    String ReturnDate, ReturnTime;

    AlertDialog.Builder builder;
    AlertDialog alertDialog;

    boolean isInternet;
    ArrayList<Vehicles> vehicleData = new ArrayList<>();
    final private static int DIALOG_VEHICLE_LIST = 1;
    //book now layout

    List<Integer> mVehicleSelList = new ArrayList<>();
    VehicleAdapter mVehicalAdapter;
    int mVehicleSelPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isInternet = new InternetStatus().isInternetOn(this);

        setUpDrawerLayout();
        setUpToolbar();
        setUpNavigationView();
        setUpNavigationFooter();
        setUpBottomLayout();
        setBookNowLayout();
        setupLayoutBottomForBooking();
        setCurrentLocation();

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        w = display.getWidth();
        h = display.getHeight();

        Log.e("w", "" + w);
        Log.e("h", "" + h);

        zoom = calculateZoomLevel(w);

       /* if (zoom < 16) {
            zoom = (float) 16;
        }*/
        Log.i("Zoom", "" + zoom);
        zoomLevel = zoom;
    }

    private void setBookNowLayout() {
        back_book_now = (ImageButton) findViewById(R.id.back_book_now);

        book_now_layout_top = (LinearLayout) findViewById(R.id.book_now_layout_top);
        pickup_layout_book_now = (LinearLayout) findViewById(R.id.pickup_layout_book_now);
        drop_layout_book_now = (LinearLayout) findViewById(R.id.drop_layout_book_now);
        layout_select = (LinearLayout) findViewById(R.id.layout_select);
        layout_select_car = (LinearLayout) findViewById(R.id.layout_select_car);
        layout_return = (LinearLayout) findViewById(R.id.layout_return);
        layout_book_car = (LinearLayout) findViewById(R.id.layout_book_car);
        layout_return_date_time = (LinearLayout) findViewById(R.id.layout_return_date_time);

        textView_pickup_title_book_now = (TextView) findViewById(R.id.textView_pickup_title_book_now);
        textView_pickup_book_now = (TextView) findViewById(R.id.textView_pickup_book_now);
        textView_drop_title_book_now = (TextView) findViewById(R.id.textView_drop_title_book_now);
        textView_drop_book_now = (TextView) findViewById(R.id.textView_drop_book_now);
        dateTime_textView_title = (TextView) findViewById(R.id.dateTime_textView_title);
        select_textView_title = (TextView) findViewById(R.id.select_textView_title);
        select_car_title = (TextView) findViewById(R.id.select_car_title);
        textView_passenger = (TextView) findViewById(R.id.textView_passenger);
        textView_total_fare_title = (TextView) findViewById(R.id.textView_total_fare_title);
        textView_total_fare = (TextView) findViewById(R.id.textView_total_fare);
        textView_book_car_title = (TextView) findViewById(R.id.textView_book_car_title);
        textView_return_back_title = (TextView) findViewById(R.id.textView_return_back_title);
        textView_time = (TextView) findViewById(R.id.textView_time);
        textView_date = (TextView) findViewById(R.id.textView_date);

        textView_pickup_title_book_now.setTypeface(setLatoRegularFont());
        textView_pickup_book_now.setTypeface(setLatoRegularFont());
        textView_drop_title_book_now.setTypeface(setLatoRegularFont());
        textView_drop_book_now.setTypeface(setLatoRegularFont());
        dateTime_textView_title.setTypeface(setLatoRegularFont());
        select_textView_title.setTypeface(setLatoRegularFont());
        select_car_title.setTypeface(setLatoRegularFont());
        textView_passenger.setTypeface(setLatoRegularFont());
        textView_total_fare_title.setTypeface(setLatoRegularFont());
        textView_total_fare.setTypeface(setLatoRegularFont());
        textView_book_car_title.setTypeface(setLatoRegularFont());
        textView_return_back_title.setTypeface(setLatoRegularFont());
        textView_time.setTypeface(setLatoRegularFont());
        textView_date.setTypeface(setLatoRegularFont());




        layout_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Date Time dialog
                DatePickerMode = 3;//Now date mode
                setDateTime(DatePickerMode);
            }
        });

        layout_select_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Car selection dialog
                if(MasterActivity.vehicleData.size() > 0)
                {
                    vehicleData = MasterActivity.vehicleData;
                    showDialog(DIALOG_VEHICLE_LIST);
                }
                else {
//                    Toast.makeText(MainActivity.this,"Else",Toast.LENGTH_LONG).show();
                    if (isInternet) {
                        sendVehicleRequest();
                    } else {
                        Toast.makeText(MainActivity.this, "No internet connection found", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        layout_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Call date time dialog
                DatePickerMode = 1; //Return date mode
//                layout_return.setVisibility(View.GONE);
                setDateTime(DatePickerMode);
            }
        });

        layout_book_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SettingsPreferences.getConsumer(MainActivity.this).getData().getUserFullName();

            }
        });

        back_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                book_now_layout_top.setVisibility(View.GONE);
                setUpToolbar();
            }
        });

        pickup_layout_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(MainActivity.this);
                    startActivityForResult(intent,PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP );
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });

        drop_layout_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(MainActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_DROP);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });
    }

    private void setUpBottomLayout() {
        pickup_layout = (LinearLayout) findViewById(R.id.pickup_layout);

        textView_pickup_title = (TextView) findViewById(R.id.textView_pickup_title);
        textView_pickup = (TextView) findViewById(R.id.textView_pickup);

        textView_pickup_title.setTypeface(setLatoSemiBoldFont());
        textView_pickup.setTypeface(setLatoSemiBoldFont());

        pickup_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(MainActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });
    }

    private void setupLayoutBottomForBooking()
    {
        layout_book_now = (LinearLayout) findViewById(R.id.layout_book_now);
        layout_book_later = (LinearLayout) findViewById(R.id.layout_book_later);


        layout_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                book_now_layout_top.setVisibility(View.VISIBLE);
                hideToolbar();
            }
        });

        layout_book_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerMode = 2; //Book later date mode
                setDateTime(DatePickerMode);
            }
        });
    }

    private void setUpDrawerLayout()
    {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        resources = getResources();
        drawerArrowDrawable = new DrawerArrowDrawable(resources);
        drawerArrowDrawable.setStrokeColor(Color.BLACK);
        drawerToggle = setupDrawerToggle();
        drawerToggle.setDrawerIndicatorEnabled(false);

        mDrawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                offset = slideOffset;

                // Sometimes slideOffset ends up so close to but not quite 1 or
                // 0.
                if (slideOffset >= .995) {
                    flipped = true;
                    drawerArrowDrawable.setFlip(flipped);
                } else if (slideOffset <= .005) {
                    flipped = false;
                    drawerArrowDrawable.setFlip(flipped);
                }

                drawerArrowDrawable.setParameter(offset);
            }
        });
    }

    private void setUpToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setVisibility(View.VISIBLE);
        toolbar.setNavigationIcon(drawerArrowDrawable);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });
    }

    private void hideToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setVisibility(View.GONE);
    }

    private void setUpNavigationFooter()
    {
        imageView_fb = (ImageView) findViewById(R.id.imageView_fb);
        imageView_twitter = (ImageView) findViewById(R.id.imageView_twitter);
        imageView_gPlus = (ImageView) findViewById(R.id.imageView_gPlus);
        imageView_linkedIn = (ImageView) findViewById(R.id.imageView_linkedIn);
        imageView_insta = (ImageView) findViewById(R.id.imageView_insta);

        imageView_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        imageView_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        imageView_gPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        imageView_linkedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        imageView_insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void setUpNavigationView()
    {
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);

        Menu menu = nvDrawer.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);

            if (menuItem != null) {

                SpannableString mNewTitle = new SpannableString(menuItem.getTitle());
                mNewTitle.setSpan(new CustomTypefaceSpan("", setLatoSemiBoldFont(),15), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                menuItem.setTitle(mNewTitle);
            }
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        switch(menuItem.getItemId()) {
            case R.id.nav_book_new:
                Intent it1 = new Intent(MainActivity.this,BookNewActivity.class);
                startActivity(it1);
                break;
            case R.id.nav_my_bookings:
                Intent it2 = new Intent(MainActivity.this,MyBookingsActivity.class);
                startActivity(it2);
                break;
            case R.id.nav_profile:
                Intent it3 = new Intent(MainActivity.this,ProfileActivity.class);
                startActivity(it3);
                break;
            case R.id.nav_refer:
                Intent it4 = new Intent(MainActivity.this,ReferActivity.class);
                startActivity(it4);
                break;
            case R.id.nav_about:
                Intent it5 = new Intent(MainActivity.this,AboutActivity.class);
                startActivity(it5);
                break;
            case R.id.nav_contact:
                Intent it6 = new Intent(MainActivity.this,ContactActivity.class);
                startActivity(it6);
                break;
            case R.id.nav_logout:
                saveLogin(0);
                SettingsPreferences.clearDB(MainActivity.this);
                Intent it7 = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(it7);
                break;
        }

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    public void showSnackBar(String msg) {
        Snackbar.make(mDrawer, msg, Snackbar.LENGTH_LONG).show();
    }

    private void setCurrentLocation() {
        /*-------------------------------------------------------------------------------------- */
        // Code for get current location on map

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            checkLocationPermission();
        }
        else
        {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            {
                showGPSDisabledAlertToUser();
            }
        }

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        /*-------------------------------------------------------------------------------------- */
    }

    private void showGPSDisabledAlertToUser()
    {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Settings", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);

                        mapFrag.getMapAsync(MainActivity.this);
                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                dialog.cancel();
            }
        });
        android.support.v7.app.AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public boolean checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION))
            {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            else
            {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        }
        else
        {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            {
                showGPSDisabledAlertToUser();
            }

            return true;
        }
    }

    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
        mGoogleApiClient.connect();
    }
    // Code for get current location on map


    // Code for drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //*********************************** calculate zoom level *****************************//
    private int calculateZoomLevel(int screenWidth) {
        double equatorLength = 40075004; // in meters
        double widthInPixels = screenWidth;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 2500) { // 3 km
            metersPerPixel /= 2.5;
            ++zoomLevel;
        }
        return zoomLevel;
    }
    //*********************************** calculate zoom level *****************************//

    private void getAddress()
    {
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Toast.makeText(MainActivity.this,""+addresses,Toast.LENGTH_SHORT).show();
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                mAddressOutput = strReturnedAddress.toString();
                textView_pickup.setText(mAddressOutput);
                Toast.makeText(MainActivity.this,mAddressOutput,Toast.LENGTH_SHORT).show();
                Log.w("Current loction address", "" + mAddressOutput);
            } else {
                Toast.makeText(MainActivity.this,"No Address returned!",Toast.LENGTH_SHORT).show();
                Log.w("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Current loction address", "Canont get Address!");
        }
    }

    //*************************** Code for date time picker *****************************/
    private void setDateTime(final int DatePickerMode)
    {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_date_time,null);

        TextView dialog_date_time_title = (TextView) layout.findViewById(R.id.dialog_date_time_title);
        TextView textView_cancel_date_time = (TextView) layout.findViewById(R.id.textView_cancel_date_time);
        TextView textView_bookNow_title_date_time = (TextView) layout.findViewById(R.id.textView_bookNow_title_date_time);

        LinearLayout layout_cancel_date_time = (LinearLayout) layout.findViewById(R.id.layout_cancel_date_time);
        LinearLayout layout_ok_date_time = (LinearLayout) layout.findViewById(R.id.layout_ok_date_time);

        SingleDateAndTimePicker single_day_picker = (SingleDateAndTimePicker) layout.findViewById(R.id.single_day_picker);

        dialog_date_time_title.setTypeface(setLatoRegularFont());
        textView_cancel_date_time.setTypeface(setLatoRegularFont());
        textView_bookNow_title_date_time.setTypeface(setLatoRegularFont());

        builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

        single_day_picker.setListener(new SingleDateAndTimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {

                Log.e("Display",""+displayed);//Tue 1 Aug 1:35 am
                Log.e("date",""+date);//Tue Aug 01 01:35:11 GMT 2017

                // 10:10 in 24 hour mode
                //12/12/2017

                switch (DatePickerMode)
                {
                    case 1: // Return date layout

                        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
                        ReturnDate = dateFormat.format(date);
                        Log.e("date",""+ReturnDate);

                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                        ReturnTime = sdf.format(date);
                        Log.e("time",""+ReturnTime);

                        break;
                    case 2: // Book later date layout
                        break;
                    case 3: //select date layout from bottom layout
                        break;
                }
            }
        });

        layout_cancel_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });

        layout_ok_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();


                switch (DatePickerMode)
                {
                    case 1: // Return date layout
                        layout_return.setVisibility(View.GONE);
                        layout_return_date_time.setVisibility(View.VISIBLE);
                        textView_time.setText(""+ReturnTime);
                        textView_date.setText(""+ReturnDate);

                        break;
                    case 2: // Book later date layout

                        break;
                    case 3: //select date layout from bottom layout

                        break;
                }
            }
        });
    }
    //*************************** Code for date time picker *****************************/

    //*************************** Code for vehicle list request ************************/
    private void sendVehicleRequest() {
        showWaitIndicator(true);

        //making object of RestAdapter
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        //Creating Rest Services
        RestInterface restInterface = adapter.create(RestInterface.class);

        //Calling method to get whether report
        restInterface.getVehicle(new Callback<VehicleList>() {

            @Override
            public void success(VehicleList model, Response response) {

                showWaitIndicator(false);

                Log.e("GET_VEHICLE_RESPONSE", "" + new Gson().toJson(model));

                if (response.getStatus() == 200) {
                    try {
                        vehicleData = model.getData();
                        MasterActivity.vehicleData = vehicleData;


                        mVehicleSelList.clear();
                        prepareVehicleSelList(MasterActivity.vehicleData.size());
                        mVehicleSelList.set(0, 1);

                        showDialog(DIALOG_VEHICLE_LIST);

                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }
    //*************************** Code for vehicle list request ************************/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause()
    {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if(mDrawer.isDrawerVisible(GravityCompat.START))
        {
            mDrawer.closeDrawers();
        }
        else
        {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            showSnackBar("Please click BACK again to exit");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null)
        {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        /*MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);*/

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel));

        getAddress();

        //stop location updates
        if (mGoogleApiClient != null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

        //Initialize Google Play Services

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
        else
        {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST_LOCATION:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    // permission was granted, yay! Do the
                    if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                        {
                            showGPSDisabledAlertToUser();
                        }

                        if (mGoogleApiClient == null)
                        {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                }
                else
                {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                latLng = place.getLatLng();

                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel));

                textView_pickup.setText(place.getName().toString());

                //stop location updates
                if (mGoogleApiClient != null)
                {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                }

                Toast.makeText(MainActivity.this,place.getName().toString(),Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP)
        {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                latLng = place.getLatLng();

//                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel));

                textView_pickup_book_now.setText(place.getName().toString());

                //stop location updates
                if (mGoogleApiClient != null)
                {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                }

                Toast.makeText(MainActivity.this,place.getName().toString(),Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_DROP)
        {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                latLng = place.getLatLng();

//                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel));

                textView_drop_book_now.setText(place.getName().toString());

                //stop location updates
                if (mGoogleApiClient != null)
                {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                }

                Toast.makeText(MainActivity.this,place.getName().toString(),Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog dialogDetails = null;

        switch (id) {
            case DIALOG_VEHICLE_LIST:
                LayoutInflater inflater = LayoutInflater.from(this);
                View dialogview = inflater.inflate(R.layout.dialog_car_selection, null);

                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
                dialogbuilder.setView(dialogview);
                dialogDetails = dialogbuilder.create();

                break;
        }

        return dialogDetails;
    }

    @Override
    protected void onPrepareDialog(int id, final Dialog dialog) {

        switch (id) {
            case DIALOG_VEHICLE_LIST:
                final AlertDialog alertDialog = (AlertDialog) dialog;
                TextView dialog_car_selection_title = (TextView) alertDialog.findViewById(R.id.dialog_car_selection_title);
                TextView textView_cancel_car_selection = (TextView) alertDialog.findViewById(R.id.textView_cancel_car_selection);

                ListView carSection_list = (ListView) alertDialog.findViewById(R.id.carSection_list);

                LinearLayout layout_cancel_car_selection = (LinearLayout) alertDialog.findViewById(R.id.layout_cancel_car_selection);
                LinearLayout layout_ok_car_selection = (LinearLayout) alertDialog.findViewById(R.id.layout_ok_car_selection);

                dialog_car_selection_title.setTypeface(setLatoRegularFont());
                textView_cancel_car_selection.setTypeface(setLatoBoldFont());


                mVehicalAdapter = new VehicleAdapter(this,vehicleData, mVehicleSelList);
                carSection_list.setAdapter(mVehicalAdapter);

                carSection_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        mVehicleSelPos = position;

                        mVehicleSelList.clear();
                        prepareVehicleSelList(vehicleData.size());
                        mVehicleSelList.set(position, 1);
                        mVehicalAdapter.notifyDataSetChanged();



                    }
                });



                layout_cancel_car_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                layout_ok_car_selection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        UpdateCarSelection();

                        dialog.dismiss();
                    }
                });
                break;
        }
    }

    public void UpdateCarSelection(){

        Vehicles data = vehicleData.get(mVehicleSelPos);


        Log.e("Car Name :", "   -"+data.getVehicleType());
        Log.e("Passengers  :", "   -"+data.getVehiclePassenger());
        Log.e("Car Luggage :", "   -"+data.getVehicleHandLuggage());

        textView_passenger.setText("  "+data.getVehiclePassenger()+"  "+data.getVehicleType());

    }

    public void prepareVehicleSelList(int size){

        if(size > 0)
        {
            for(int i=0 ; i <size ; i++){

                mVehicleSelList.add(0);

            Log.e("size che v","    :"+i);
            }
        }

        Log.e("size che v","    :"+mVehicleSelList.size());

    }
}
