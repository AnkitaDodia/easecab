package com.easecab.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.easecab.R;
import com.easecab.common.MasterActivity;
import com.easecab.models.Login;
import com.easecab.models.Registration;
import com.easecab.restinterface.RestInterface;
import com.easecab.utils.InternetStatus;
import com.easecab.utils.SettingsPreferences;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Ankita on 21-Apr-17.
 */
public class RegisterActivity extends MasterActivity
{
    TextView textView_register_str,textView_signup_title;

    EditText edit_name_register,edit_phone_register,edit_email_register,edit_pass_register;

    Button button_register;

    ImageButton register_back;

    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);

        isInternet = new InternetStatus().isInternetOn(this);

        initView();
        setClicks();
    }

    private void initView()
    {
        textView_register_str = (TextView) findViewById(R.id.textView_register_str);
        textView_signup_title = (TextView) findViewById(R.id.textView_title);

        button_register = (Button) findViewById(R.id.button_register);

        register_back = (ImageButton) findViewById(R.id.back);

        edit_name_register = (EditText) findViewById(R.id.edit_name_register);
        edit_phone_register = (EditText) findViewById(R.id.edit_phone_register);
        edit_email_register = (EditText) findViewById(R.id.edit_email_register);
        edit_pass_register = (EditText) findViewById(R.id.edit_pass_register);

        textView_signup_title.setText(R.string.register);

        textView_register_str.setTypeface(setLatoRegularFont());
        textView_signup_title.setTypeface(setLatoRegularFont());
        button_register.setTypeface(setLatoRegularFont());
        edit_name_register.setTypeface(setLatoRegularFont());
        edit_phone_register.setTypeface(setLatoRegularFont());
        edit_email_register.setTypeface(setLatoRegularFont());
        edit_pass_register.setTypeface(setLatoRegularFont());
    }

    private void setClicks() {
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edit_name_register.getText().toString().length() == 0)
                {
                    Toast.makeText(RegisterActivity.this, "Please enter your name", Toast.LENGTH_LONG).show();
                }
                else if(edit_phone_register.getText().toString().length() == 0)
                {
                    Toast.makeText(RegisterActivity.this, "Please enter mobile number", Toast.LENGTH_LONG).show();
                }
                else if(edit_email_register.getText().toString().length() == 0)
                {
                    Toast.makeText(RegisterActivity.this, "Please enter email id", Toast.LENGTH_LONG).show();
                }
                else if(edit_pass_register.getText().toString().length() == 0)
                {
                    Toast.makeText(RegisterActivity.this, "Please enter password", Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isInternet) {
                        sendRegisterRequest();
                    } else {
                        Toast.makeText(RegisterActivity.this, "No internet connection found", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        register_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(it);
            }
        });
    }

    private void sendRegisterRequest()
    {
        showWaitIndicator(true);

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS); // connect timeout

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL)
                .setClient(new OkClient(client)).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.sendRegisterRequest(edit_name_register.getText().toString(), edit_phone_register.getText().toString(),
                edit_email_register.getText().toString(), edit_pass_register.getText().toString(),new Callback<Registration>() {
            @Override
            public void success(Registration model, Response response) {
                showWaitIndicator(false);

                if (response.getStatus() == 200) {
                    try {
                        Log.e("REGISTER_RESPONSE", "" + new Gson().toJson(model));

                        Toast.makeText(RegisterActivity.this, "Register successfully", Toast.LENGTH_LONG).show();

                        Intent it = new Intent(RegisterActivity.this,LoginActivity.class);
                        startActivity(it);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());

                if (error.equals(null) || error.getMessage().contains("Unable to resolve host")) {
                    Toast.makeText(RegisterActivity.this, "Something went wrong, please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getMessage().contains("failed to connect")) {
                    Toast.makeText(RegisterActivity.this, "Sorry can not connect to server please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getResponse().getStatus() == 423) {
                    Toast.makeText(RegisterActivity.this, error.getResponse().getReason(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
