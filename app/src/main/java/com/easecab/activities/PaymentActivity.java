package com.easecab.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easecab.R;
import com.easecab.common.MasterActivity;

/**
 * Created by Ankita on 23-May-17.
 */
public class PaymentActivity extends MasterActivity
{
    ImageButton back_green;

    TextView textView_title_green;

    TextView textView_flight_detail_title,textView_profile_displayName_title,textView_displayName,textView_flightNo_title,
            textView_flightNo,textView_flightFrom_title,textView_flightFrom,textView_payment_method_title,textView_cash,
            textView_paypal,textView_confirm_booking;

    LinearLayout layout_cash,layout_payPal,layout_confirm_booking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_payment);

        initView();
    }

    private void initView() {
        back_green = (ImageButton) findViewById(R.id.back_green);

        layout_cash = (LinearLayout) findViewById(R.id.layout_cash);
        layout_payPal = (LinearLayout) findViewById(R.id.layout_payPal);
        layout_confirm_booking = (LinearLayout) findViewById(R.id.layout_confirm_booking);

        textView_title_green = (TextView) findViewById(R.id.textView_title_green);

        textView_flight_detail_title = (TextView) findViewById(R.id.textView_flight_detail_title);
        textView_profile_displayName_title = (TextView) findViewById(R.id.textView_profile_displayName_title);
        textView_displayName = (TextView) findViewById(R.id.textView_displayName);
        textView_flightNo_title = (TextView) findViewById(R.id.textView_flightNo_title);
        textView_flightNo = (TextView) findViewById(R.id.textView_flightNo);
        textView_flightFrom_title = (TextView) findViewById(R.id.textView_flightFrom_title);
        textView_flightFrom = (TextView) findViewById(R.id.textView_flightFrom);
        textView_payment_method_title = (TextView) findViewById(R.id.textView_payment_method_title);
        textView_cash = (TextView) findViewById(R.id.textView_cash);
        textView_paypal = (TextView) findViewById(R.id.textView_paypal);
        textView_confirm_booking = (TextView) findViewById(R.id.textView_confirm_booking);

        textView_title_green.setText(R.string.bookingDetail);
        textView_title_green.setTypeface(setLatoRegularFont());

        textView_flight_detail_title.setTypeface(setLatoBoldFont());
        textView_profile_displayName_title.setTypeface(setLatoRegularFont());
        textView_displayName.setTypeface(setLatoBoldFont());
        textView_flightNo_title.setTypeface(setLatoRegularFont());
        textView_flightNo.setTypeface(setLatoBoldFont());
        textView_flightFrom_title.setTypeface(setLatoRegularFont());
        textView_flightFrom.setTypeface(setLatoBoldFont());
        textView_payment_method_title.setTypeface(setLatoSemiBoldFont());
        textView_cash.setTypeface(setLatoBoldFont());
        textView_paypal.setTypeface(setLatoBoldFont());
        textView_confirm_booking.setTypeface(setLatoBoldFont());

        back_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layout_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        layout_payPal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        layout_confirm_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
