package com.easecab.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easecab.R;
import com.easecab.common.MasterActivity;

/**
 * Created by Ankita on 02-May-17.
 */
public class ProfileActivity extends MasterActivity
{
    ImageButton back_green;

    TextView textView_title_green;

    TextView textView_personal_detail_title,textView_profile_full_name_title,textView_profile_full_name,textView_profile_email_title,
            textView_profile_email,textView_profile_contact_title,textView_profile_contact,textView_security_title,
            textView_profile_change_pass_title;

    LinearLayout layout_change_pass;

    final private static int DIALOG_CHANGE_PASS = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        initView();
    }

    private void initView() {
        back_green = (ImageButton) findViewById(R.id.back_green);

        textView_title_green = (TextView) findViewById(R.id.textView_title_green);
        textView_personal_detail_title = (TextView) findViewById(R.id.textView_personal_detail_title);
        textView_profile_full_name_title = (TextView) findViewById(R.id.textView_profile_full_name_title);
        textView_profile_full_name = (TextView) findViewById(R.id.textView_profile_full_name);
        textView_profile_email_title = (TextView) findViewById(R.id.textView_profile_email_title);
        textView_profile_email = (TextView) findViewById(R.id.textView_profile_email);
        textView_profile_contact_title = (TextView) findViewById(R.id.textView_profile_contact_title);
        textView_profile_contact = (TextView) findViewById(R.id.textView_title_green);
        textView_security_title = (TextView) findViewById(R.id.textView_security_title);
        textView_profile_change_pass_title = (TextView) findViewById(R.id.textView_profile_change_pass_title);

        textView_title_green.setText(R.string.profile);
        textView_title_green.setTypeface(setLatoRegularFont());

        textView_personal_detail_title.setTypeface(setLatoSemiBoldFont());
        textView_profile_full_name_title.setTypeface(setLatoRegularFont());
        textView_profile_full_name.setTypeface(setLatoRegularFont());
        textView_profile_email_title.setTypeface(setLatoRegularFont());
        textView_profile_email.setTypeface(setLatoRegularFont());
        textView_profile_contact_title.setTypeface(setLatoRegularFont());
        textView_profile_contact.setTypeface(setLatoRegularFont());
        textView_security_title.setTypeface(setLatoSemiBoldFont());
        textView_profile_change_pass_title.setTypeface(setLatoRegularFont());

        layout_change_pass = (LinearLayout) findViewById(R.id.layout_change_pass);

        back_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layout_change_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_CHANGE_PASS);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog dialogDetails = null;

        switch (id) {
            case DIALOG_CHANGE_PASS:
                LayoutInflater inflater = LayoutInflater.from(this);
                View dialogview = inflater.inflate(R.layout.dialog_change_pass, null);

                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
                dialogbuilder.setView(dialogview);
                dialogDetails = dialogbuilder.create();

                break;
        }

        return dialogDetails;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {

        switch (id) {
            case DIALOG_CHANGE_PASS:
                final AlertDialog alertDialog = (AlertDialog) dialog;
                TextView dialog_title = (TextView) alertDialog.findViewById(R.id.dialog_title);
                TextView textView_cancel = (TextView)alertDialog.findViewById(R.id.textView_cancel);
                TextView textView_bookNow_title = (TextView) alertDialog.findViewById(R.id.textView_bookNow_title);

                EditText edit_new_pass = (EditText) alertDialog.findViewById(R.id.edit_new_pass);
                EditText edit_cofirm_pass = (EditText) alertDialog.findViewById(R.id.edit_cofirm_pass);

                LinearLayout layout_cancel = (LinearLayout) alertDialog.findViewById(R.id.layout_cancel);
                LinearLayout layout_ok = (LinearLayout) alertDialog.findViewById(R.id.layout_ok);

                dialog_title.setTypeface(setLatoBoldFont());
                textView_cancel.setTypeface(setLatoBoldFont());
                textView_bookNow_title.setTypeface(setLatoBoldFont());

                edit_new_pass.setTypeface(setLatoRegularFont());
                edit_cofirm_pass.setTypeface(setLatoRegularFont());

                layout_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                layout_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                break;
        }
    }
}
