package com.easecab.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.easecab.R;
import com.easecab.common.MasterActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ankita on 20-May-17.
 */
public class GoogleAddressActivity extends MasterActivity implements PlaceSelectionListener
{
    PlaceAutocompleteFragment autocompleteFragment;

    LatLng latLong;

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_address);


        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(GoogleAddressActivity.this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if( mGoogleApiClient != null )
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.i(TAG, "Place Selected: " + place.getName());

        MasterActivity.place = place;

        MasterActivity.latitude = latLong.latitude;
        MasterActivity.longitude = latLong.longitude;

        finish();

        /*if (mMap != null) {
            latLong = place.getLatLng();

            Log.e(TAG, "latLong Selected: " + latLong);

            mMapIsTouched = true;
            markerClicked = false;
            MasterActivity.latitude = latLong.latitude;
            MasterActivity.longitude = latLong.longitude;

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLong).zoom(zoom).tilt(0).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }*/
    }

    @Override
    public void onError(Status status) {
        Log.e(TAG, "onError: Status = " + status.toString());

        Toast.makeText(this, "Place selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }
}
