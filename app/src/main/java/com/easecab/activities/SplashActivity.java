package com.easecab.activities;

import android.content.Intent;
import android.os.Bundle;

import com.easecab.R;
import com.easecab.common.MasterActivity;

/**
 * Created by Ankita on 21-Apr-17.
 */
public class SplashActivity extends MasterActivity
{
    protected int splashTime = 2000;
    private Thread splashTread;
    SplashActivity sPlashScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        sPlashScreen = this;
        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(splashTime);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if(getLogin() != 0)
                    {
                        Intent i = new Intent();
                        i.setClass(sPlashScreen, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else
                    {
                        Intent i = new Intent();
                        i.setClass(sPlashScreen, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        };
        splashTread.start();
    }
}
