package com.easecab.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.easecab.R;
import com.easecab.adapters.BookingAdapter;
import com.easecab.common.MasterActivity;
import com.easecab.models.Booking;
import com.easecab.models.BookingList;
import com.easecab.models.Login;
import com.easecab.restinterface.RestInterface;
import com.easecab.utils.InternetStatus;
import com.easecab.utils.SettingsPreferences;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Ankita on 02-May-17.
 */
public class MyBookingsActivity extends MasterActivity
{
    ImageButton back_green;

    TextView textView_title_green,textView_current_booking_title,textView_pat_booking_title,no_current_data,no_past_data;

    ListView current_booking_list,past_booking_list;

    boolean isInternet;

    String UserId;

    ArrayList<Booking> currentBookings = new ArrayList<>();
    ArrayList<Booking> completeBookings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_bookings);

        isInternet = new InternetStatus().isInternetOn(this);

        initView();

        UserId = SettingsPreferences.getConsumer(this).getData().getUserId();

        if(masterCurrentBookings.size() > 0 && masterCompleteBookings.size() > 0)
        {
            currentBookings = masterCurrentBookings;
            completeBookings = masterCompleteBookings;

            setBookingData();
        }
        else
        {
            if (isInternet) {
                getCurrentBookingRequest();
                getCompleteBookingRequest();
            } else {
                Toast.makeText(MyBookingsActivity.this, "No internet connection found", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void initView() {
        back_green = (ImageButton) findViewById(R.id.back_green);

        textView_title_green = (TextView) findViewById(R.id.textView_title_green);
        textView_current_booking_title = (TextView) findViewById(R.id.textView_current_booking_title);
        textView_pat_booking_title = (TextView) findViewById(R.id.textView_pat_booking_title);
        no_current_data = (TextView) findViewById(R.id.no_current_data);
        no_past_data = (TextView) findViewById(R.id.no_past_data);

        current_booking_list = (ListView) findViewById(R.id.current_booking_list);
        past_booking_list = (ListView) findViewById(R.id.past_booking_list);

        textView_title_green.setText(R.string.myBookings);
        textView_title_green.setTypeface(setLatoRegularFont());

        textView_current_booking_title.setTypeface(setLatoSemiBoldFont());
        textView_pat_booking_title.setTypeface(setLatoSemiBoldFont());
        no_current_data.setTypeface(setLatoSemiBoldFont());
        no_past_data.setTypeface(setLatoSemiBoldFont());

        back_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getCurrentBookingRequest()
    {
        showWaitIndicator(true);

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS); // connect timeout

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL)
                .setClient(new OkClient(client)).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getCurrentBookingRequest(UserId, new Callback<BookingList>() {
            @Override
            public void success(BookingList model, Response response) {
                showWaitIndicator(false);

                if (response.getStatus() == 200) {
                    try {
                        Log.e("CURRENT_RESPONSE", "" + new Gson().toJson(model));

                        showWaitIndicator(false);

                        currentBookings = model.getData();
                        masterCurrentBookings = currentBookings;

                        setBookingData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());

                if (error.equals(null) || error.getMessage().contains("Unable to resolve host")) {
                    Toast.makeText(MyBookingsActivity.this, "Something went wrong, please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getMessage().contains("failed to connect")) {
                    Toast.makeText(MyBookingsActivity.this, "Sorry can not connect to server please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getResponse().getStatus() == 423) {
                    Toast.makeText(MyBookingsActivity.this, error.getResponse().getReason(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getCompleteBookingRequest()
    {
//        showWaitIndicator(true);

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(60, TimeUnit.SECONDS); // connect timeout

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL)
                .setClient(new OkClient(client)).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getCompleteBookingRequest(UserId, new Callback<BookingList>() {
            @Override
            public void success(BookingList model, Response response) {
//                showWaitIndicator(false);

                if (response.getStatus() == 200) {
                    try {
                        Log.e("COMPLETE_RESPONSE", "" + new Gson().toJson(model));

//                        showWaitIndicator(false);
                        completeBookings = model.getData();
                        masterCompleteBookings = completeBookings;

                        setBookingData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

//                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());

                if (error.equals(null) || error.getMessage().contains("Unable to resolve host")) {
                    Toast.makeText(MyBookingsActivity.this, "Something went wrong, please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getMessage().contains("failed to connect")) {
                    Toast.makeText(MyBookingsActivity.this, "Sorry can not connect to server please try after some time..", Toast.LENGTH_LONG).show();
                } else if (error.getResponse().getStatus() == 423) {
                    Toast.makeText(MyBookingsActivity.this, error.getResponse().getReason(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setBookingData()
    {
        if(currentBookings.size() > 0)
        {
            current_booking_list.setVisibility(View.VISIBLE);
            no_current_data.setVisibility(View.GONE);

            BookingAdapter adapter = new BookingAdapter(this,currentBookings);
            current_booking_list.setAdapter(adapter);
        }
        else
        {
            current_booking_list.setVisibility(View.GONE);
            no_current_data.setVisibility(View.VISIBLE);
        }

        Toast.makeText(MyBookingsActivity.this,""+completeBookings.size(),Toast.LENGTH_LONG).show();
        if(completeBookings.size() > 0)
        {
            past_booking_list.setVisibility(View.VISIBLE);
            no_past_data.setVisibility(View.GONE);

            BookingAdapter adapter1 = new BookingAdapter(this,completeBookings);
            past_booking_list.setAdapter(adapter1);
        }
        else
        {
            past_booking_list.setVisibility(View.GONE);
            no_past_data.setVisibility(View.VISIBLE);
        }
    }
}
