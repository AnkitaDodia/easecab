package com.easecab.restinterface;

import com.easecab.models.BookingList;
import com.easecab.models.ForgotPassword;
import com.easecab.models.Login;
import com.easecab.models.Registration;
import com.easecab.models.VehicleList;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by Adite-Ankita on 6/30/2016.
 */
public interface RestInterface
{

    String API_BASE_URL = "http://www.thewebsolutions.in/my-html/android"; //Live host

    String LOGIN = "/login.php?action=login&";
    String REGISTER = "/login.php?action=signup&";
    String FORGOT_PASSWORD = "/login.php?action=forgotPassword&";
    String CURRENT_BOOKING = "/ocm.api.php?action=currentBooking&";
    String COMPLETE_BOOKING = "/ocm.api.php?action=completeBooking&";
    String VEHICLE_LIST = "/ocm.api.php?action=vehicleList";

    //Send Login
    @FormUrlEncoded
    @POST(LOGIN)
    public void sendLoginRequest(@Field("userName") String userName, @Field("password") String password, Callback<Login> callBack);


    //Registration
    @FormUrlEncoded
    @POST(REGISTER)
    public void sendRegisterRequest(@Field("fullName") String fullName, @Field("phoneNo") String phoneNo,
                                    @Field("email") String email, @Field("password") String password,
                                    Callback<Registration> callBack);

    //Forgot Password
    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    public void sendForgotPassRequest(@Field("email") String email, Callback<ForgotPassword> callback);

    //Current Booking
    @FormUrlEncoded
    @POST(CURRENT_BOOKING)
    public void getCurrentBookingRequest(@Field("userId") String email, Callback<BookingList> callback);

    //Complete Booking
    @FormUrlEncoded
    @POST(COMPLETE_BOOKING)
    public void getCompleteBookingRequest(@Field("userId") String email, Callback<BookingList> callback);

    //Get traffic type
    @GET(VEHICLE_LIST)
    public void getVehicle(Callback<VehicleList> callback);
}
